const express = require('express');

const app = express();
const mongoose = require('mongoose');
const port = process.env.PORT || 3000; // Server port

//connect mongoose database
const db = require('./db/db');

const path = require('path');
const { send } = require('process');

//in Models
const Todo = require('./models/Todo')

const hbs = require('hbs');

//call to body-parser
const bodyParse = require('body-parser');
const { urlencoded } = require('body-parser');
const { exception } = require('console');
//init body parser
app.use(bodyParse.json(),urlencoded({ extended: true }));
//call files
app.use('/assets',express.static(path.join(__dirname,'public')));

//views [npm install hbs --save]
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');
//!!! !!! !!! !!! Routers !!! !!! !!! !!!

app.get('/',(req,res) => {
    let TodoData = [];
    //get Todo s Data
        Todo.find()
        .then((todos) => {

            TodoData = todos;
            console.log(TodoData);
            //Send Todo data To Index
            res.render('index',{
                pageTitle: 'Home',
                dataTodo: TodoData,
            });
        })
        .catch(() => {
            throw new exception('Error');
        });
   
});
app.get('/create',(req,res) => {
    res.render('create',{pageTitle:'Create Note'});
});

app.post('/save',(req,res) => {
    //get title and body not
        //console.log(req.body);
        const { title,body } = req.body;
        // const title = req.body.title;
        // const body = req.body.body;
    //save todo in DB
    Todo.create({title:title,  description:body},(err,todo) => { 
        if(err){
            return res.redirect('/create');
        }else res.redirect('/');//redirect to index
   
    });
    
});

app.get('/show/:id',(req,res) => {
    let todoSelected = [];
    let id = req.params.id;

    // check if id is valid 
    isInvalidId(res,id);

    Todo.findById(id).then((Todo) => {
        todoSelected = Todo;
        res.render('show',{
            pageTitle:'Show Notes',
            data: todoSelected
            });
    }).catch(()=>{
        throw new exception('Error');
    });
});
app.get('/edit/:id',(req,res) => {
    let todoSelected = [];
    let id = req.params.id;

    // check if id is valid 
    isInvalidId(res,id);
    
    Todo.findById(id).then((Todo) => {
        todoSelected = Todo;
         res.render('edit',{
             pageTitle:'Edit Note',
             data: todoSelected
            });
    }).catch(()=>{
        throw new exception('Error');
    });
});
app.post('/update/:id',(req,res) => {
    const { body, title } = req.body;
    let id = req.params.id;

    // check if id is valid 
    isInvalidId(res,id);
    
    Todo.findByIdAndUpdate(id,{title:title,description:body}).then(() => {
        res.redirect(`/show/${id}`);
    }).catch((err) => {
        console.log(err);
    });
});
app.post('/remove/:id',(req,res) => {
    let id = req.params.id;

    // check if id is valid 
    isInvalidId(res,id);
    
    Todo.findByIdAndDelete(id).then(() => {
        res.redirect('/');
    }).catch((err) => {console.log(err)});

});
app.post('/update/:id/false-status',(req,res) => {
    let id = req.params.id;

    // check if id is valid 
    isInvalidId(res,id);
    
    const todo = Todo.findById(id);
    Todo.updateOne({_id: id}, {completed: false}).then(() => {
        res.redirect(`/show/${id}`);
    }).catch((err) => {
        console.log(err);
    });
});

app.post('/update/:id/true-status',(req,res) => {
    //
    const id = req.params.id;
    const todo = Todo.findById(id);

    Todo.updateOne({_id: id}, {completed: true}).then(() => {
        res.redirect(`/show/${id}`);
    }).catch((err) => {
        console.log(err);
    });
});
//check if id is valid
const isInvalidId = (res,id) => {
    if(! mongoose.isValidObjectId(id)){
        res.render('404',{
            pageTitle:'404 - page not Found',
        });
    }
}
//!!! !!! !!! !!! run serve !!! !!! !!! !!! 
app.listen(port,(err) => {
    console.log('Errors : ' + err);
    console.log('server is listen in port 3000 .');
});