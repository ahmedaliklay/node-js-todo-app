const mongoose = require('mongoose');
//process.env.DB_URI
const db = mongoose.connect('mongodb://localhost:27017/ToDoApp',{useNewUrlParser:true,useUnifiedTopology:true});

mongoose.connection.once('open',()=>{
    console.log('database is connect');
});

module.exports = {db};